%ifarch aarch64
%global disable_bcc 1
%global _with_bcc --with-pmdabcc=no
%global disable_mssql 1
%else
%global disable_bcc 0
%global _with_bcc --with-pmdabcc=yes
%global disable_mssql 0
%endif
%global disable_dstat 0
%global __python3 python

%global _confdir        %{_sysconfdir}/pcp
%global _logsdir        %{_localstatedir}/log/pcp
%global _pmnsdir        %{_localstatedir}/lib/pcp/pmns
%global _pmnsexecdir    %{_libexecdir}/pcp/pmns
%global _tempsdir       %{_localstatedir}/lib/pcp/tmp
%global _pmdasdir       %{_localstatedir}/lib/pcp/pmdas
%global _pmdasexecdir   %{_libexecdir}/pcp/pmdas
%global _testsdir       %{_localstatedir}/lib/pcp/testsuite
%global _selinuxdir     %{_localstatedir}/lib/pcp/selinux
%global _selinuxexecdir %{_libexecdir}/pcp/selinux
%global _logconfdir     %{_localstatedir}/lib/pcp/config/pmlogconf
%global _ieconfdir      %{_localstatedir}/lib/pcp/config/pmieconf
%global _tapsetdir      %{_datadir}/systemtap/tapset
%global _bashcompdir    %{_datadir}/bash-completion/completions
%global _pixmapdir      %{_datadir}/pcp-gui/pixmaps
%global _hicolordir     %{_datadir}/icons/hicolor
%global _booksdir       %{_datadir}/doc/pcp-doc

%global _with_doc --with-docdir=%{_docdir}/%{name}

%global _with_dstat --with-dstat-symlink=yes

%global _initddir %{_libexecdir}/pcp/lib

%global _with_ib --with-infiniband=yes

%global _with_perfevent --with-perfevent=yes

%global _with_podman --with-podman=yes

%global _with_bpf --with-pmdabpf=yes

%global _with_bpftrace --with-pmdabpftrace=yes

%global _with_json --with-pmdajson=yes

%global _with_nutcracker --with-pmdanutcracker=yes

%global _with_snmp --with-pmdasnmp=yes

Name:             pcp
Version:          5.3.7
Summary:          System-level performance monitoring and performance management
Release:          2
License:          GPL-2.0-or-later and LGPL-2.0-or-later and CC-BY-SA-3.0
URL:              https://pcp.io
Source0:          https://github.com/performancecopilot/pcp/archive/refs/tags/5.3.7.tar.gz
BuildRequires: make
BuildRequires: gcc gcc-c++
BuildRequires: procps autoconf bison flex
BuildRequires: nss-devel
BuildRequires: avahi-devel
BuildRequires: xz-devel
BuildRequires: zlib-devel
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: ncurses-devel
BuildRequires: readline-devel
BuildRequires: cyrus-sasl-devel
BuildRequires: libpfm-devel >= 4
BuildRequires: systemtap-sdt-devel
BuildRequires: libuv-devel >= 1.0
BuildRequires: openssl-devel >= 1.1.1
BuildRequires: perl-generators
BuildRequires: perl-devel perl(strict)
BuildRequires: perl(ExtUtils::MakeMaker) perl(LWP::UserAgent) perl(JSON)
BuildRequires: perl(Time::HiRes) perl(Digest::MD5)
BuildRequires: perl(XML::LibXML) perl(File::Slurp)
BuildRequires: man hostname
BuildRequires: systemd-devel
BuildRequires: desktop-file-utils
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtsvg-devel

Requires: bash xz gawk sed grep findutils which hostname
Requires: pcp-libs = %{version}-%{release}
Requires: pcp-conf = %{version}-%{release}
Requires: pcp-selinux = %{version}-%{release}

Provides: pcp-libs = %{version}-%{release}
Provides: pcp-pmda-kvm = %{version}-%{release}
Provides: pcp-webapi = %{version}-%{release}

Conflicts: librapi < 0.16

Obsoletes: pcp-pmda-kvm < %{version}-%{release}
Obsoletes: pcp-libs < %{version}-%{release}
Obsoletes: pcp-pmda-rpm < 5.3.2
Obsoletes: pcp-pmda-rpm-debuginfo < 5.3.2
Obsoletes: pcp-webapi-debuginfo < 5.0.0
Obsoletes: pcp-webapi < 5.0.0
Obsoletes: pcp-manager-debuginfo < 5.2.0
Obsoletes: pcp-manager < 5.2.0
Obsoletes: pcp-compat
Obsoletes: pcp-monitor < 4.2.0
Obsoletes: pcp-collector < 4.2.0
Obsoletes: pcp-pmda-nvidia

%global pmda_remove() %{expand:
if [ %1 -eq 0 ]
then
    PCP_PMDAS_DIR=%{_pmdasdir}
    PCP_PMCDCONF_PATH=%{_confdir}/pmcd/pmcd.conf
    if [ -f "$PCP_PMCDCONF_PATH" -a -f "$PCP_PMDAS_DIR/%2/domain.h" ]
    then
        (cd "$PCP_PMDAS_DIR/%2/" && ./Remove >/dev/null 2>&1)
    fi
fi
}

%global install_file() %{expand:
if [ -w "%1" ]
then
    (cd "%1" && touch "%2" && chmod 644 "%2")
else
    echo "WARNING: Cannot write to %1, skipping %2 creation." >&2
fi
}

%global rebuild_pmns() %{expand:
if [ -w "%1" ]
then
    (cd "%1" && ./Rebuild -s && rm -f "%2")
else
    echo "WARNING: Cannot write to %1, skipping namespace rebuild." >&2
fi
}

%global selinux_handle_policy() %{expand:
if [ %1 -ge 1 ]
then
    %{_libexecdir}/pcp/bin/selinux-setup %{_selinuxdir} install %2
elif [ %1 -eq 0 ]
then
    %{_libexecdir}/pcp/bin/selinux-setup %{_selinuxdir} remove %2
fi
}

%description
Performance Co-Pilot (PCP) provides a framework and services to support
system-level performance monitoring and performance management.

The PCP open source release provides a unifying abstraction for all of
the interesting performance data in a system, and allows client
applications to easily retrieve and process any subset of that data.

%package conf
Summary:          Performance Co-Pilot run-time configuration
License:          LGPLv2+
Conflicts:        pcp-libs < 3.9
%description conf
Performance Co-Pilot (PCP) run-time configuration

%package devel
Summary:          Performance Co-Pilot (PCP) development tools and documentation
License:          GPLv2+ and LGPLv2+
Requires:         pcp = %{version}-%{release}
Provides:         pcp-libs-devel = %{version}-%{release} pcp-testsuite = %{version}-%{release}
Obsoletes:        pcp-libs-devel < %{version}-%{release} pcp-testsuite < %{version}-%{release} pcp-gui-testsuite
Requires:         pcp-pmda-activemq pcp-pmda-bonding pcp-pmda-dbping pcp-pmda-ds389 pcp-pmda-ds389log
Requires:         pcp-pmda-elasticsearch pcp-pmda-gpfs pcp-pmda-gpsd pcp-pmda-lustre
Requires:         pcp-pmda-memcache pcp-pmda-mysql pcp-pmda-named pcp-pmda-netfilter pcp-pmda-news
Requires:         pcp-pmda-nginx pcp-pmda-nfsclient pcp-pmda-pdns pcp-pmda-postfix pcp-pmda-postgresql
Requires:         pcp-pmda-samba pcp-pmda-slurm pcp-pmda-zimbra pcp-pmda-dm pcp-pmda-apache
Requires:         pcp-pmda-bash pcp-pmda-cisco pcp-pmda-gfs2 pcp-pmda-mailq pcp-pmda-mounts
Requires:         pcp-pmda-nvidia-gpu pcp-pmda-roomtemp pcp-pmda-sendmail pcp-pmda-shping pcp-pmda-smart
Requires:         pcp-pmda-hacluster pcp-pmda-lustrecomm pcp-pmda-logger pcp-pmda-denki pcp-pmda-docker
Requires:         pcp-pmda-sockets pcp-pmda-podman pcp-pmda-nutcracker pcp-pmda-bpf pcp-pmda-bpftrace
Requires:         pcp-pmda-gluster pcp-pmda-zswap pcp-pmda-unbound pcp-pmda-mic
Requires:         pcp-pmda-libvirt pcp-pmda-lio pcp-pmda-openmetrics pcp-pmda-haproxy
Requires:         pcp-pmda-lmsensors pcp-pmda-netcheck pcp-pmda-rabbitmq pcp-pmda-oracle
Requires:         pcp-pmda-openvswitch pcp-pmda-mongodb pcp-pmda-snmp pcp-pmda-bind2
Requires:         pcp-pmda-json pcp-pmda-summary pcp-pmda-trace pcp-pmda-weblog
Requires:         pcp-system-tools pcp-gui %{_vendor}-rpm-config bc gcc gzip bzip2
Requires:         selinux-policy-devel selinux-policy-targeted setools-console
%if !%{disable_bcc}
Requires: pcp-pmda-bcc
%endif
%if !%{disable_mssql}
Requires: pcp-pmda-mssql 
%endif
%description devel
Performance Co-Pilot (PCP) documentation and tools for development.

%package help
Summary:          Documents for %{name}
Buildarch:        noarch
Requires:         man info
Provides:         pcp-doc = %{version}-%{release}
Obsoletes:        pcp-doc < %{version}-%{release}
%description help
Man pages and other related documents for %{name}.

%package -n perl-PCP-PMDA
Summary:          Performance Co-Pilot (PCP) Perl bindings and documentation
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-PMDA
The PCP::PMDA Perl module contains the language bindings for
building Performance Metric Domain Agents (PMDAs) using Perl.
Each PMDA exports performance data for one specific domain, for
example the operating system kernel, Cisco routers, a database,
an application, etc.

%package -n perl-PCP-MMV
Summary:          Performance Co-Pilot (PCP) Perl bindings for PCP Memory Mapped Values
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-MMV
The PCP::MMV module contains the Perl language bindings for
building scripts instrumented with the Performance Co-Pilot
(PCP) Memory Mapped Value (MMV) mechanism.
This mechanism allows arbitrary values to be exported from an
instrumented script into the PCP infrastructure for monitoring
and analysis with pmchart, pmie, pmlogger and other PCP tools.

%package -n perl-PCP-LogImport
Summary:          Performance Co-Pilot (PCP) Perl bindings for importing external data into PCP archives
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-LogImport
The PCP::LogImport module contains the Perl language bindings for
importing data in various 3rd party formats into PCP archives so
they can be replayed with standard PCP monitoring tools.

%package -n perl-PCP-LogSummary
Summary:          Performance Co-Pilot (PCP) Perl bindings for post-processing output of pmlogsummary
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter

%description -n perl-PCP-LogSummary
The PCP::LogSummary module provides a Perl module for using the
statistical summary data produced by the Performance Co-Pilot
pmlogsummary utility.  This utility produces various averages,
minima, maxima, and other calculations based on the performance
data stored in a PCP archive.  The Perl interface is ideal for
exporting this data into third-party tools (e.g. spreadsheets).

%package import-sar2pcp
Summary:          Performance Co-Pilot tools for importing sar data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release} perl(XML::TokeParser)
%description import-sar2pcp
Performance Co-Pilot (PCP) front-end tools for importing sar data
into standard PCP archive logs for replay with any PCP monitoring tool.

%package import-iostat2pcp
Summary:          Performance Co-Pilot tools for importing iostat data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}
%description import-iostat2pcp
Performance Co-Pilot (PCP) front-end tools for importing iostat data
into standard PCP archive logs for replay with any PCP monitoring tool.

%package import-mrtg2pcp
Summary:          Performance Co-Pilot tools for importing MTRG data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}
%description import-mrtg2pcp
Performance Co-Pilot (PCP) front-end tools for importing MTRG data
into standard PCP archive logs for replay with any PCP monitoring tool.

%package import-ganglia2pcp
Summary:          Performance Co-Pilot tools for importing ganglia data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}

%description import-ganglia2pcp
Performance Co-Pilot (PCP) front-end tools for importing ganglia data
into standard PCP archive logs for replay with any PCP monitoring tool.

%package import-collectl2pcp
Summary:          Performance Co-Pilot tools for importing collectl log files into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release}

%description import-collectl2pcp
Performance Co-Pilot (PCP) front-end tools for importing collectl data
into standard PCP archive logs for replay with any PCP monitoring tool.

%package export-zabbix-agent
Summary:          Module that can export PCP indicators to Zabbix agent
License:          GPLv2+
Requires:         pcp = %{version}-%{release}

%description export-zabbix-agent
Performance Co-Pilot (PCP) module for exporting metrics from PCP to
Zabbix via the Zabbix agent - see zbxpcp(3) for further details.

%package export-pcp2elasticsearch
Summary: Performance Co-Pilot tools for exporting PCP metrics to ElasticSearch
License: GPLv2+
Requires: pcp >= %{version}-%{release} python3-pcp = %{version}-%{release} python3-requests
BuildRequires: python3-requests
%description export-pcp2elasticsearch
Performance Co-Pilot (PCP) front-end tools for exporting metric values
to Elasticsearch - a distributed, RESTful search and analytics engine.
See https://www.elastic.co/community for further details.

%package export-pcp2graphite
Summary:          Performance Co-Pilot tools for exporting PCP metrics to Graphite
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}
%description export-pcp2graphite
Performance Co-Pilot (PCP) front-end tools for exporting metric values
to graphite (http://graphite.readthedocs.org).


%package export-pcp2influxdb
Summary:          Performance Co-Pilot tools for exporting PCP metrics to InfluxDB
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release} python3-requests
%description export-pcp2influxdb
Performance Co-Pilot (PCP) front-end tools for exporting metric values
to InfluxDB (https://influxdata.com/time-series-platform/influxdb).

%package export-pcp2json
Summary:          Performance Co-Pilot tools for exporting PCP metrics in JSON format
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2json
Performance Co-Pilot (PCP) front-end tools for exporting metric values
in JSON format.

%package export-pcp2spark
Summary:          Performance Co-Pilot tools for exporting PCP metrics to Apache Spark
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2spark
Performance Co-Pilot (PCP) front-end tools for exporting metric values
in JSON format to Apache Spark. See https://spark.apache.org/ for
further details on Apache Spark.

%package export-pcp2xml
Summary:          Performance Co-Pilot tools for exporting PCP metrics in XML format
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2xml
Performance Co-Pilot (PCP) front-end tools for exporting metric values
in XML format.

%package export-pcp2zabbix
Summary:          Performance Co-Pilot tools for exporting PCP metrics to Zabbix
License:          GPLv2+
Requires:         python3-pcp = %{version}-%{release} pcp >= %{version}-%{release}

%description export-pcp2zabbix
Performance Co-Pilot (PCP) front-end tools for exporting metric values
to the Zabbix (https://www.zabbix.org/) monitoring software.

%package pmda-podman
Summary: Performance Co-Pilot (PCP) metrics for podman containers
License: GPLv2+
Requires: pcp = %{version}-%{release}
%description pmda-podman
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting podman container and pod statistics via the podman REST API.

%package pmda-perfevent
Summary:          Performance Co-Pilot (PCP) metrics for hardware counters
License:          GPLv2+
Requires:         pcp = %{version}-%{release} libpfm >= 4
BuildRequires:    libpfm-devel >= 4
Obsoletes:        pcp-pmda-papi < 5.0.0
%description pmda-perfevent
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting hardware counters statistics through libpfm.

%package pmda-infiniband
Summary:          Performance Co-Pilot (PCP) metrics for Infiniband HCAs and switches
License:          GPLv2+
Requires:         pcp = %{version}-%{release} libibmad >= 1.3.7 libibumad >= 1.3.7
BuildRequires:    libibmad-devel >= 1.3.7 libibumad-devel >= 1.3.7
%description pmda-infiniband
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting Infiniband statistics.  By default, it monitors the local HCAs
but can also be configured to monitor remote GUIDs such as IB switches.

%package pmda-activemq
Summary:          Performance Co-Pilot (PCP) metrics for ActiveMQ
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent) pcp = %{version}-%{release}
%description pmda-activemq
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the ActiveMQ message broker.

%package pmda-bind2
License: GPLv2+
Summary: Performance Co-Pilot (PCP) metrics for BIND servers
URL: https://pcp.io
Requires: pcp = %{version}-%{release} pcp-libs = %{version}-%{release}
Requires: perl-PCP-PMDA = %{version}-%{release}
Requires: perl(LWP::UserAgent)
Requires: perl(XML::LibXML)
Requires: perl(File::Slurp)
Requires: perl-autodie
Requires: perl-Time-HiRes
%description pmda-bind2
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from BIND (Berkeley Internet Name Domain).

%package pmda-redis
Summary:          Performance Co-Pilot (PCP) metrics for Redis
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
Requires:         perl-autodie perl-Time-HiRes perl-Data-Dumper
%description pmda-redis
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from Redis servers (redis.io).

%package pmda-nutcracker
Summary: Performance Co-Pilot (PCP) metrics for NutCracker (TwemCache)
License: GPLv2+
Requires: pcp = %{version}-%{release} perl-PCP-PMDA = %{version}-%{release} perl(YAML::XS::LibYAML) perl(JSON)
%description pmda-nutcracker
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from NutCracker (TwemCache).

%package pmda-bonding
Summary:          Performance Co-Pilot (PCP) metrics for Bonded network interfaces
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-bonding
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about bonded network interfaces.

%package pmda-dbping
Summary:          Performance Co-Pilot (PCP) metrics for Database response times and Availablility
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl-DBI pcp = %{version}-%{release}
%description pmda-dbping
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Database response times and Availablility.

%package pmda-ds389
Summary:          Performance Co-Pilot (PCP) metrics for 389 Directory Servers
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-ds389
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about a 389 Directory Server.

%package pmda-ds389log
Summary:          Performance Co-Pilot (PCP) metrics for 389 Directory Server Loggers
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl-Date-Manip pcp = %{version}-%{release}
%description pmda-ds389log
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from a 389 Directory Server log.

%package pmda-elasticsearch
Summary: Performance Co-Pilot (PCP) metrics for Elasticsearch
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp
%description pmda-elasticsearch
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Elasticsearch.

%package pmda-gpfs
Summary:          Performance Co-Pilot (PCP) metrics for GPFS Filesystem
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-gpfs
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the GPFS filesystem.

%package pmda-gpsd
Summary:          Performance Co-Pilot (PCP) metrics for a GPS Daemon
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-gpsd
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about a GPS Daemon.

%package pmda-denki
Summary: Performance Co-Pilot (PCP) metrics dealing with electrical power
License: GPLv2+
Requires: pcp = %{version}-%{release}
%description pmda-denki
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics related to the electrical power consumed by and inside
the system.

%package pmda-docker
Summary:          Performance Co-Pilot (PCP) metrics from the Docker daemon
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-docker
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics using the Docker daemon REST API.

%package pmda-lustre
Summary:          Performance Co-Pilot (PCP) metrics for the Lustre Filesytem
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-lustre
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Lustre Filesystem.

%package pmda-lustrecomm
Summary:          Performance Co-Pilot (PCP) metrics for the Lustre Filesytem Comms
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-lustrecomm
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Lustre Filesystem Comms.

%package pmda-memcache
Summary:          Performance Co-Pilot (PCP) metrics for Memcached
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-memcache
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Memcached.

%package pmda-mysql
Summary:          Performance Co-Pilot (PCP) metrics for MySQL
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(DBI) perl(DBD::mysql) pcp = %{version}-%{release}
BuildRequires:    perl(DBI) perl(DBD::mysql)
%description pmda-mysql
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the MySQL database.

%package pmda-named
Summary:          Performance Co-Pilot (PCP) metrics for Named
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-named
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Named nameserver.

%package pmda-netfilter
Summary:          Performance Co-Pilot (PCP) metrics for Netfilter framework
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-netfilter
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Netfilter packet filtering framework.

%package pmda-news
Summary:          Performance Co-Pilot (PCP) metrics for Usenet News
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-news
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Usenet News.

%package pmda-nginx
Summary:          Performance Co-Pilot (PCP) metrics for the Nginx Webserver
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent) pcp = %{version}-%{release}
BuildRequires:    perl(LWP::UserAgent)
%description pmda-nginx
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Nginx Webserver.

%package pmda-nfsclient
Summary:          Performance Co-Pilot (PCP) metrics for NFS Clients
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release} python3-pcp
%description pmda-nfsclient
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics for NFS Clients.

%package pmda-oracle
Summary:          Performance Co-Pilot (PCP) metrics for the Oracle database
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(DBI) pcp = %{version}-%{release}
BuildRequires:    perl(DBI)
%description pmda-oracle
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Oracle database.

%package pmda-pdns
Summary:          Performance Co-Pilot (PCP) metrics for PowerDNS
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl-Time-HiRes pcp = %{version}-%{release}
%description pmda-pdns
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the PowerDNS.

%package pmda-postfix
Summary:          Performance Co-Pilot (PCP) metrics for the Postfix (MTA)
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} postfix-perl-scripts pcp = %{version}-%{release} perl-Time-HiRes
BuildRequires:    postfix-perl-scripts
%description pmda-postfix
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Postfix (MTA).

%package pmda-postgresql
Summary:          Performance Co-Pilot (PCP) metrics for PostgreSQL
License:          GPLv2+
Requires:         python3-pcp python3-psycopg2 pcp = %{version}-%{release}
BuildRequires:    python3-psycopg2
%description pmda-postgresql
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the PostgreSQL database.

%package pmda-rsyslog
Summary:          Performance Co-Pilot (PCP) metrics for Rsyslog
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-rsyslog
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Rsyslog.

%package pmda-samba
Summary:          Performance Co-Pilot (PCP) metrics for Samba
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-samba
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Samba.

%package pmda-slurm
Summary:          Performance Co-Pilot (PCP) metrics for the SLURM Workload Manager
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-slurm
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from the SLURM Workload Manager.

%package pmda-snmp
Summary:          Performance Co-Pilot (PCP) metrics for Simple Network Management Protocol
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-snmp
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about SNMP.

%package pmda-zimbra
Summary:          Performance Co-Pilot (PCP) metrics for Zimbra
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} pcp = %{version}-%{release}
%description pmda-zimbra
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Zimbra.

%package pmda-dm
Summary:          Performance Co-Pilot (PCP) metrics for the Device Mapper Cache and Thin Client
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
BuildRequires:    device-mapper-devel
%description pmda-dm
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Device Mapper Cache and Thin Client.

%if !%{disable_bcc}
%package pmda-bcc
Summary:          Performance Co-Pilot (PCP) metrics from eBPF/BCC modul
License:          ASL 2.0 and GPLv2+
Requires:         python3-bpfcc python3-pcp pcp = %{version}-%{release}
%description pmda-bcc
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting performance metrics from eBPF/BCC Python modules.
%endif

%package pmda-bpf
Summary: Performance Co-Pilot (PCP) metrics from eBPF ELF modules
License: ASL 2.0 and GPLv2+
Requires: pcp = %{version}-%{release} libbpf
BuildRequires: libbpf-devel clang llvm bpftool
%description pmda-bpf
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting performance metrics from eBPF ELF modules.

%package pmda-bpftrace
Summary: Performance Co-Pilot (PCP) metrics from bpftrace scripts
License: ASL 2.0 and GPLv2+
Requires: pcp = %{version}-%{release} bpftrace >= 0.9.2 python3-pcp python3 >= 3.6
%description pmda-bpftrace
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting performance metrics from bpftrace scripts.

%package pmda-gluster
Summary:          Performance Co-Pilot (PCP) metrics for the Gluster filesystem
License:          GPLv2+
Requires:         python3-pcp pcp = %{version}-%{release}
%description pmda-gluster
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the gluster filesystem.

%package pmda-zswap
Summary:          Performance Co-Pilot (PCP) metrics for compressed swap
License:          GPLv2+
Requires:         python3-pcp pcp = %{version}-%{release}
%description pmda-zswap
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about compressed swap.

%package pmda-unbound
Summary:          Performance Co-Pilot (PCP) metrics for the Unbound DNS Resolver
License:          GPLv2+
Requires:         python3-pcp pcp = %{version}-%{release}
%description pmda-unbound
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Unbound DNS Resolver.

%package pmda-mic
Summary:          Performance Co-Pilot (PCP) metrics for Intel MIC cards
License:          GPLv2+
Requires:         python3-pcp pcp = %{version}-%{release}
%description pmda-mic
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Intel MIC cards.

%package pmda-haproxy
Summary:          Performance Co-Pilot (PCP) metrics for HAProxy
License:          GPLv2+
Requires:         python3-pcp pcp = %{version}-%{release}
%description pmda-haproxy
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting performance metrics from HAProxy over the HAProxy stats socket.

%package pmda-libvirt
Summary:          Performance Co-Pilot (PCP) metrics from virtual machines
License:          GPLv2+
Requires:         python3-pcp libvirt-python3 python3-lxml
BuildRequires:    libvirt-python3 python3-lxml
%description pmda-libvirt
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting virtualisation statistics from libvirt about behaviour of guest
and hypervisor machines.

%package pmda-openvswitch
Summary: Performance Co-Pilot (PCP) metrics for Open vSwitch
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp
%description pmda-openvswitch
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from Open vSwitch.

%package pmda-rabbitmq
Summary: Performance Co-Pilot (PCP) metrics for RabbitMQ queues
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp
%description pmda-rabbitmq
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about RabbitMQ message queues.

%package pmda-lio
Summary:          Performance Co-Pilot (PCP) metrics for the LIO subsystem
License:          GPLv2+
Requires:         python3-pcp python3-rtslib pcp = %{version}-%{release}
BuildRequires:    python3-rtslib
%description pmda-lio
This package provides a PMDA to gather performance metrics from the kernels
iSCSI target interface (LIO). The metrics are stored by LIO within the Linux
kernels configfs filesystem. The PMDA provides per LUN level stats, and a
summary instance per iSCSI target, which aggregates all LUN metrics within the
target.

%package pmda-openmetrics
Summary: Performance Co-Pilot (PCP) metrics from OpenMetrics endpoints
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp python3-requests
BuildRequires: python3-requests
Obsoletes: pcp-pmda-prometheus < 5.0.0 pcp-pmda-vmware < 5.3.5
Provides: pcp-pmda-prometheus < 5.0.0
%description pmda-openmetrics
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
extracting metrics from OpenMetrics (https://openmetrics.io/) endpoints.

%package pmda-netcheck
Summary: Performance Co-Pilot (PCP) metrics for simple network checks
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp
%description pmda-netcheck
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from simple network checks.

%package pmda-mongodb
Summary: Performance Co-Pilot (PCP) metrics for MongoDB
License: GPLv2+
Requires: pcp = %{version}-%{release} python3-pcp
%description pmda-mongodb
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from MongoDB.

%if !%{disable_mssql}
%package pmda-mssql
License: GPLv2+
Summary: Performance Co-Pilot (PCP) metrics for Microsoft SQL Server
URL: https://pcp.io
Requires: pcp = %{version}-%{release} pcp-libs = %{version}-%{release}
Requires: python3-pcp
%description pmda-mssql
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from Microsoft SQL Server.
%endif

%package pmda-json
Summary:          Performance Co-Pilot (PCP) metrics for JSON data
License:          GPLv2+
Requires:         python3-pcp python3-jsonpointer python3-six
BuildRequires:    python3-jsonpointer python3-six
%description pmda-json
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics output in JSON.

%package pmda-apache
Summary:          Performance Co-Pilot (PCP) metrics for the Apache webserver
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-apache
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Apache webserver.

%package pmda-bash
Summary:          Performance Co-Pilot (PCP) metrics for the Bash shell
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-bash
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Bash shell.

%package pmda-cifs
Summary:          Performance Co-Pilot (PCP) metrics for the CIFS protocol
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-cifs
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Common Internet Filesytem.

%package pmda-cisco
Summary:          Performance Co-Pilot (PCP) metrics for Cisco routers
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-cisco
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Cisco routers.

%package pmda-gfs2
Summary:          Performance Co-Pilot (PCP) metrics for the GFS2 filesystem
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-gfs2
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Global Filesystem v2.

%package pmda-lmsensors
Summary:          Performance Co-Pilot (PCP) metrics for hardware sensors
License:          GPLv2+
Requires:         pcp = %{version}-%{release} python3-pcp lm_sensors
%description pmda-lmsensors
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the Linux hardware monitoring sensors.

%package pmda-logger
Summary:          Performance Co-Pilot (PCP) metrics from arbitrary log files
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-logger
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from a specified set of log files (or pipes).  The PMDA
supports both sampled and event-style metrics.

%package pmda-mailq
Summary:          Performance Co-Pilot (PCP) metrics for the sendmail queue
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-mailq
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about email queues managed by sendmail.

%package pmda-mounts
Summary:          Performance Co-Pilot (PCP) metrics for filesystem mounts
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-mounts
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about filesystem mounts.

%package pmda-nvidia-gpu
Summary:          Performance Co-Pilot (PCP) metrics for the Nvidia GPU
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-nvidia-gpu
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Nvidia GPUs.

%package pmda-roomtemp
Summary:          Performance Co-Pilot (PCP) metrics for the room temperature
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-roomtemp
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about the room temperature.

%package pmda-sendmail
Summary:          Performance Co-Pilot (PCP) metrics for Sendmail
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-sendmail
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about Sendmail traffic.

%package pmda-shping
Summary:          Performance Co-Pilot (PCP) metrics for shell command responses
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-shping
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about quality of service and response time measurements of
arbitrary shell commands.

%package pmda-smart
Summary:          Performance Co-Pilot (PCP) metrics for S.M.A.R.T values
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-smart
This package contains the PCP Performance Metric Domain Agent (PMDA) for
collecting metrics of disk S.M.A.R.T values making use of data from the
smartmontools package.

%package pmda-sockets
Summary: Performance Co-Pilot (PCP) per-socket metrics
License: GPLv2+
Requires: pcp = %{version}-%{release}
Requires: iproute
%description pmda-sockets
This package contains the PCP Performance Metric Domain Agent (PMDA) for
collecting per-socket statistics, making use of utilities such as 'ss'.

%package pmda-hacluster
Summary: Performance Co-Pilot (PCP) metrics for High Availability Clusters
License: GPLv2+
Requires: pcp = %{version}-%{release}
%description pmda-hacluster
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about linux High Availability (HA) Clusters.

%package pmda-summary
Summary:          Performance Co-Pilot (PCP) summary metrics from pmie
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-summary
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about other installed PMDAs.

%package pmda-systemd
Summary:          Performance Co-Pilot (PCP) metrics from the Systemd journal
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-systemd
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics from the Systemd journal.

%package pmda-trace
Summary:          Performance Co-Pilot (PCP) metrics for application tracing
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-trace
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about trace performance data in applications.

%package pmda-weblog
Summary:          Performance Co-Pilot (PCP) metrics from web server logs
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-weblog
This package contains the PCP Performance Metrics Domain Agent (PMDA) for
collecting metrics about web server logs.

%package zeroconf
Summary:          Performance Co-Pilot (PCP) Zeroconf Package
License:          GPLv2+
Requires:         pcp pcp-doc pcp-system-tools pcp-pmda-dm pcp-pmda-nfsclient pcp-pmda-openmetrics
%description zeroconf
This package contains configuration tweaks and files to increase metrics
gathering frequency, several extended pmlogger configurations, as well as
automated pmie diagnosis, alerting and self-healing for the localhost.

%package -n python3-pcp
Summary:          Performance Co-Pilot (PCP) Python3 bindings and documentation
License:          GPLv2+
Requires:         pcp = %{version}-%{release} python3
%description -n python3-pcp
This package contains language bindings for the Performance Metric API (PMAPI) monitoring
tool and Performance Metric Domain Agent (PMDA) collector tool written in Python3.

%package system-tools
Summary:          Performance Co-Pilot (PCP) System and Monitoring Tools
License:          GPLv2+
Requires:         python3-pcp = %{version}-%{release} pcp = %{version}-%{release}
Requires:         pcp-zeroconf = %{version}-%{release}
Provides:         /usr/bin/dstat dstat = %{version}-%{release}
%description system-tools
This PCP module contains additional system monitoring tools written
in the Python language.

%package gui
Summary:          Visualization tools for the Performance Co-Pilot toolkit
License:          GPLv2+ and LGPLv2+ and LGPLv2+ with exceptions
BuildRequires:    hicolor-icon-theme
Requires:         pcp = %{version}-%{release} liberation-sans-fonts
%description gui
Visualization tools for the Performance Co-Pilot toolkit.
The pcp-gui package primarily includes visualization tools for
monitoring systems using live and archived Performance Co-Pilot
(PCP) sources.

%package selinux
Summary:          PCP selinux policy
License:          GPLv2+ and CC-BY
BuildRequires:    selinux-policy-devel selinux-policy-targeted setools-console
Requires:         policycoreutils selinux-policy-targeted
%description selinux
This package contains SELinux support for PCP.  The package contains
interface rules, type enforcement and file context adjustments for an
updated policy package.

%prep
%autosetup -p1


%build
%global __strip %{_builddir}/%{?buildsubdir}/build/rpm/custom-strip

_build=`echo %{release} | sed -e 's/\..*$//'`
sed -i "/PACKAGE_BUILD/s/=[0-9]*/=$_build/" VERSION.pcp

%configure %{?_with_initd} %{?_with_doc} %{?_with_dstat} %{?_with_ib} %{?_with_podman} %{?_with_perfevent} %{?_with_bcc} %{?_with_bpf} %{?_with_bpftrace} %{?_with_json} %{?_with_snmp} %{?_with_nutcracker}
make %{?_smp_mflags} default_pcp

%install

rm -Rf $RPM_BUILD_ROOT
BACKDIR=`pwd`
NO_CHOWN=true
DIST_ROOT=$RPM_BUILD_ROOT
DIST_TMPFILES=$BACKDIR/install.tmpfiles
DIST_MANIFEST=$BACKDIR/install.manifest
export NO_CHOWN DIST_ROOT DIST_MANIFEST DIST_TMPFILES
rm -f $DIST_MANIFEST $DIST_TMPFILES
make install_pcp

rm -f $RPM_BUILD_ROOT/%{_libdir}/*.a
sed -i -e '/\.a$/d' $DIST_MANIFEST

rm -f $RPM_BUILD_ROOT/%{_bindir}/sheet2pcp $RPM_BUILD_ROOT/%{_mandir}/man1/sheet2pcp.1*
sed -i -e '/sheet2pcp/d' $DIST_MANIFEST

rm -f $RPM_BUILD_ROOT/%{_includedir}/pcp/configsz.h
sed -i -e '/configsz.h/d' $DIST_MANIFEST
rm -f $RPM_BUILD_ROOT/%{_includedir}/pcp/platformsz.h
sed -i -e '/platformsz.h/d' $DIST_MANIFEST

%if %{disable_mssql}
rm -fr $RPM_BUILD_ROOT/%{_confdir}/mssql
rm -fr $RPM_BUILD_ROOT/%{_confdir}/pmieconf/mssql
rm -fr $RPM_BUILD_ROOT/%{_ieconfdir}/mssql
rm -fr $RPM_BUILD_ROOT/%{_pmdasdir}/mssql
rm -fr $RPM_BUILD_ROOT/%{_pmdasexecdir}/mssql
%endif

desktop-file-validate $RPM_BUILD_ROOT/%{_datadir}/applications/pmchart.desktop

for f in $RPM_BUILD_ROOT/%{_initddir}/{pcp,pmcd,pmlogger,pmie,pmproxy}; do
    test -f "$f" || continue
    sed -i -e '/^# chkconfig/s/:.*$/: - 95 05/' -e '/^# Default-Start:/s/:.*$/:/' $f
done

PCP_GUI='pmchart|pmconfirm|pmdumptext|pmmessage|pmquery|pmsnap|pmtime'

PCP_CONF=$BACKDIR/src/include/pcp.conf
export PCP_CONF
. $BACKDIR/src/include/pcp.env
CFGFILELIST=`ls -1 $BACKDIR/debian/pcp-conf.{install,dirs}`
LIBFILELIST=`ls -1 $BACKDIR/debian/lib*.{install,dirs} | grep -F -v -- -dev.`
DEVFILELIST=`ls -1 $BACKDIR/debian/lib*-dev.{install,dirs}`

sed -e 's/^/\//' $CFGFILELIST >pcp-conf-files
sed -e 's/^/\//' $LIBFILELIST >pcp-libs-files
sed -e 's/^/\//' $DEVFILELIST >pcp-devel-files
grep "\.h$" $DEVFILELIST | cut -f2 -d":" >pcp-libs-devel-files
grep "\.pc$" $DEVFILELIST | cut -f2 -d":" >>pcp-libs-devel-files
grep "\.so$" $DEVFILELIST | cut -f2 -d":" >>pcp-libs-devel-files
grep "\.a$" $DEVFILELIST | cut -f2 -d":" >>pcp-libs-devel-files
sed -i -e 's/^/\//' pcp-libs-devel-files
sed -i '/.h$/d' pcp-devel-files
sed -i '/.pc$/d' pcp-devel-files
sed -i '/.so$/d' pcp-devel-files
sed -i '/.a$/d' pcp-devel-files
sed -i '/\/man\//d' pcp-devel-files
sed -i '/\/include\//d' pcp-devel-files


sed -i -e 's/usr\/lib\//usr\/lib64\//' pcp-libs-files
sed -i -e 's/usr\/lib\//usr\/lib64\//' pcp-devel-files
sed -i -e 's/usr\/lib\//usr\/lib64\//' pcp-libs-devel-files


awk '{print $NF}' $DIST_MANIFEST |\
grep -E 'pcp\/(examples|demos)|(etc/pcp|pcp/pmdas)\/(sample|simple|trivial|txmon)|bin/(pmdbg|pmclient|pmerr|genpmda)' | grep -E -v tutorials >>pcp-devel-files

cat >confpath.list <<EOF
etc/zabbix/zabbix_agentd.d/
etc/sysconfig/
etc/cron.d/
etc/sasl2/
etc/pcp/
EOF

keep() {
    grep -E $@ || return 0
}
cull() {
    grep -E -v $@ || return 0
}
total_manifest() {
    awk '{print $NF}' $DIST_MANIFEST
}
basic_manifest() {
    total_manifest | cull '/pcp-help/|/testsuite/|/man/|/examples/'
}

total_manifest | keep 'tutorials|/html/|pcp-help|man.*\.[1-9].*' | cull 'out' >pcp-help-files
total_manifest | keep 'testsuite|etc/systemd/system|libpcp_fault|pcp/fault.h' >pcp-testsuite-files

basic_manifest | keep "$PCP_GUI|pcp-gui|applications|pixmaps|hicolor" | cull 'pmtime.h' >pcp-gui-files
basic_manifest | keep 'selinux' | cull 'tmp|GNUselinuxdefs' >pcp-selinux-files
basic_manifest | keep 'zeroconf|daily[-_]report|/sa$' >pcp-zeroconf-files
basic_manifest | grep -E -e 'pmiostat|pmrep|dstat|htop|pcp2csv' \
   -e 'pcp-atop|pcp-dmcache|pcp-dstat|pcp-free|pcp-htop' \
   -e 'pcp-ipcs|pcp-iostat|pcp-lvmcache|pcp-mpstat' \
   -e 'pcp-numastat|pcp-pidstat|pcp-shping|pcp-tapestat' \
   -e 'pcp-uptime|pcp-verify|pcp-ss' | \
   cull 'selinux|pmlogconf|pmieconf|pmrepconf' >pcp-system-tools-files

basic_manifest | keep 'sar2pcp' >pcp-import-sar2pcp-files
basic_manifest | keep 'iostat2pcp' >pcp-import-iostat2pcp-files
basic_manifest | keep 'sheet2pcp' >pcp-import-sheet2pcp-files
basic_manifest | keep 'mrtg2pcp' >pcp-import-mrtg2pcp-files
basic_manifest | keep 'ganglia2pcp' >pcp-import-ganglia2pcp-files
basic_manifest | keep 'collectl2pcp' >pcp-import-collectl2pcp-files
basic_manifest | keep 'pcp2elasticsearch' >pcp-export-pcp2elasticsearch-files
basic_manifest | keep 'pcp2influxdb' >pcp-export-pcp2influxdb-files
basic_manifest | keep 'pcp2graphite' >pcp-export-pcp2graphite-files
basic_manifest | keep 'pcp2json' >pcp-export-pcp2json-files
basic_manifest | keep 'pcp2spark' >pcp-export-pcp2spark-files
basic_manifest | keep 'pcp2xml' >pcp-export-pcp2xml-files
basic_manifest | keep 'pcp2zabbix' >pcp-export-pcp2zabbix-files
basic_manifest | keep 'zabbix|zbxpcp' | cull pcp2zabbix >pcp-export-zabbix-agent-files
basic_manifest | keep '(etc/pcp|pmdas)/activemq(/|$)' >pcp-pmda-activemq-files
basic_manifest | keep '(etc/pcp|pmdas)/apache(/|$)' >pcp-pmda-apache-files
basic_manifest | keep '(etc/pcp|pmdas)/bash(/|$)' >pcp-pmda-bash-files
basic_manifest | keep '(etc/pcp|pmdas)/bcc(/|$)' >pcp-pmda-bcc-files
basic_manifest | keep '(etc/pcp|pmdas)/bind2(/|$)' >pcp-pmda-bind2-files
basic_manifest | keep '(etc/pcp|pmdas)/bonding(/|$)' >pcp-pmda-bonding-files
basic_manifest | keep '(etc/pcp|pmdas)/bpf(/|$)' >pcp-pmda-bpf-files
basic_manifest | keep '(etc/pcp|pmdas)/bpftrace(/|$)' >pcp-pmda-bpftrace-files
basic_manifest | keep '(etc/pcp|pmdas)/cifs(/|$)' >pcp-pmda-cifs-files
basic_manifest | keep '(etc/pcp|pmdas)/cisco(/|$)' >pcp-pmda-cisco-files
basic_manifest | keep '(etc/pcp|pmdas)/dbping(/|$)' >pcp-pmda-dbping-files
basic_manifest | keep '(etc/pcp|pmdas|pmieconf)/dm(/|$)' >pcp-pmda-dm-files
basic_manifest | keep '(etc/pcp|pmdas)/denki(/|$)' >pcp-pmda-denki-files
basic_manifest | keep '(etc/pcp|pmdas)/docker(/|$)' >pcp-pmda-docker-files
basic_manifest | keep '(etc/pcp|pmdas)/ds389log(/|$)' >pcp-pmda-ds389log-files
basic_manifest | keep '(etc/pcp|pmdas)/ds389(/|$)' >pcp-pmda-ds389-files
basic_manifest | keep '(etc/pcp|pmdas)/elasticsearch(/|$)' >pcp-pmda-elasticsearch-files
basic_manifest | keep '(etc/pcp|pmdas)/gfs2(/|$)' >pcp-pmda-gfs2-files
basic_manifest | keep '(etc/pcp|pmdas)/gluster(/|$)' >pcp-pmda-gluster-files
basic_manifest | keep '(etc/pcp|pmdas)/gpfs(/|$)' >pcp-pmda-gpfs-files
basic_manifest | keep '(etc/pcp|pmdas)/gpsd(/|$)' >pcp-pmda-gpsd-files
basic_manifest | keep '(etc/pcp|pmdas)/hacluster(/|$)' >pcp-pmda-hacluster-files
basic_manifest | keep '(etc/pcp|pmdas)/haproxy(/|$)' >pcp-pmda-haproxy-files
basic_manifest | keep '(etc/pcp|pmdas)/infiniband(/|$)' >pcp-pmda-infiniband-files
basic_manifest | keep '(etc/pcp|pmdas)/json(/|$)' >pcp-pmda-json-files
basic_manifest | keep '(etc/pcp|pmdas)/libvirt(/|$)' >pcp-pmda-libvirt-files
basic_manifest | keep '(etc/pcp|pmdas)/lio(/|$)' >pcp-pmda-lio-files
basic_manifest | keep '(etc/pcp|pmdas)/lmsensors(/|$)' >pcp-pmda-lmsensors-files
basic_manifest | keep '(etc/pcp|pmdas)/logger(/|$)' >pcp-pmda-logger-files
basic_manifest | keep '(etc/pcp|pmdas)/lustre(/|$)' >pcp-pmda-lustre-files
basic_manifest | keep '(etc/pcp|pmdas)/lustrecomm(/|$)' >pcp-pmda-lustrecomm-files
basic_manifest | keep '(etc/pcp|pmdas)/memcache(/|$)' >pcp-pmda-memcache-files
basic_manifest | keep '(etc/pcp|pmdas)/mailq(/|$)' >pcp-pmda-mailq-files
basic_manifest | keep '(etc/pcp|pmdas)/mic(/|$)' >pcp-pmda-mic-files
basic_manifest | keep '(etc/pcp|pmdas)/mounts(/|$)' >pcp-pmda-mounts-files
basic_manifest | keep '(etc/pcp|pmdas)/mongodb(/|$)' >pcp-pmda-mongodb-files
basic_manifest | keep '(etc/pcp|pmdas|pmieconf)/mssql(/|$)' >pcp-pmda-mssql-files
basic_manifest | keep '(etc/pcp|pmdas)/mysql(/|$)' >pcp-pmda-mysql-files
basic_manifest | keep '(etc/pcp|pmdas)/named(/|$)' >pcp-pmda-named-files
basic_manifest | keep '(etc/pcp|pmdas)/netfilter(/|$)' >pcp-pmda-netfilter-files
basic_manifest | keep '(etc/pcp|pmdas)/netcheck(/|$)' >pcp-pmda-netcheck-files
basic_manifest | keep '(etc/pcp|pmdas)/news(/|$)' >pcp-pmda-news-files
basic_manifest | keep '(etc/pcp|pmdas)/nfsclient(/|$)' >pcp-pmda-nfsclient-files
basic_manifest | keep '(etc/pcp|pmdas)/nginx(/|$)' >pcp-pmda-nginx-files
basic_manifest | keep '(etc/pcp|pmdas)/nutcracker(/|$)' >pcp-pmda-nutcracker-files
basic_manifest | keep '(etc/pcp|pmdas)/nvidia(/|$)' >pcp-pmda-nvidia-files
basic_manifest | keep '(etc/pcp|pmdas)/openmetrics(/|$)' >pcp-pmda-openmetrics-files
basic_manifest | keep '(etc/pcp|pmdas)/openvswitch(/|$)' >pcp-pmda-openvswitch-files
basic_manifest | keep '(etc/pcp|pmdas)/oracle(/|$)' >pcp-pmda-oracle-files
basic_manifest | keep '(etc/pcp|pmdas)/pdns(/|$)' >pcp-pmda-pdns-files
basic_manifest | keep '(etc/pcp|pmdas)/perfevent(/|$)' >pcp-pmda-perfevent-files
basic_manifest | keep '(etc/pcp|pmdas)/podman(/|$)' >pcp-pmda-podman-files
basic_manifest | keep '(etc/pcp|pmdas)/postfix(/|$)' >pcp-pmda-postfix-files
basic_manifest | keep '(etc/pcp|pmdas)/postgresql(/|$)' >pcp-pmda-postgresql-files
basic_manifest | keep '(etc/pcp|pmdas)/rabbitmq(/|$)' >pcp-pmda-rabbitmq-files
basic_manifest | keep '(etc/pcp|pmdas)/redis(/|$)' >pcp-pmda-redis-files
basic_manifest | keep '(etc/pcp|pmdas)/roomtemp(/|$)' >pcp-pmda-roomtemp-files
basic_manifest | keep '(etc/pcp|pmdas)/rpm(/|$)' >pcp-pmda-rpm-files
basic_manifest | keep '(etc/pcp|pmdas)/rsyslog(/|$)' >pcp-pmda-rsyslog-files
basic_manifest | keep '(etc/pcp|pmdas)/samba(/|$)' >pcp-pmda-samba-files
basic_manifest | keep '(etc/pcp|pmdas)/sendmail(/|$)' >pcp-pmda-sendmail-files
basic_manifest | keep '(etc/pcp|pmdas)/shping(/|$)' >pcp-pmda-shping-files
basic_manifest | keep '(etc/pcp|pmdas)/slurm(/|$)' >pcp-pmda-slurm-files
basic_manifest | keep '(etc/pcp|pmdas)/smart(/|$)' >pcp-pmda-smart-files
basic_manifest | keep '(etc/pcp|pmdas)/snmp(/|$)' >pcp-pmda-snmp-files
basic_manifest | keep '(etc/pcp|pmdas)/sockets(/|$)' >pcp-pmda-sockets-files
basic_manifest | keep '(etc/pcp|pmdas)/summary(/|$)' >pcp-pmda-summary-files
basic_manifest | keep '(etc/pcp|pmdas)/systemd(/|$)' >pcp-pmda-systemd-files
basic_manifest | keep '(etc/pcp|pmdas)/trace(/|$)' >pcp-pmda-trace-files
basic_manifest | keep '(etc/pcp|pmdas)/unbound(/|$)' >pcp-pmda-unbound-files
basic_manifest | keep '(etc/pcp|pmdas)/weblog(/|$)' >pcp-pmda-weblog-files
basic_manifest | keep '(etc/pcp|pmdas)/zimbra(/|$)' >pcp-pmda-zimbra-files
basic_manifest | keep '(etc/pcp|pmdas)/zswap(/|$)' >pcp-pmda-zswap-files

rm -f packages.list
for pmda_package in \
    activemq apache \
    bash bcc bind2 bonding bpf bpftrace \
    cifs cisco \
    dbping denki docker dm ds389 ds389log \
    elasticsearch \
    gfs2 gluster gpfs gpsd \
    hacluster haproxy \
    infiniband \
    json \
    libvirt lio lmsensors logger lustre lustrecomm \
    mailq memcache mic mounts mongodb mssql mysql \
    named netcheck netfilter news nfsclient nginx \
    nutcracker nvidia \
    openmetrics openvswitch oracle \
    pdns perfevent podman postfix postgresql \
    rabbitmq redis roomtemp rpm rsyslog \
    samba sendmail shping slurm smart snmp \
    sockets summary systemd \
    unbound \
    trace \
    weblog \
    zimbra zswap ; \
do \
    pmda_packages="$pmda_packages pcp-pmda-$pmda_package"; \
done

for import_package in \
    collectl2pcp iostat2pcp ganglia2pcp mrtg2pcp sar2pcp sheet2pcp ; \
do \
    import_packages="$import_packages pcp-import-$import_package"; \
done

for export_package in \
    pcp2elasticsearch pcp2graphite pcp2influxdb pcp2json \
    pcp2spark pcp2xml pcp2zabbix zabbix-agent ; \
do \
    export_packages="$export_packages pcp-export-$export_package"; \
done

for subpackage in \
    pcp-conf pcp-gui pcp-help pcp-libs pcp-devel pcp-libs-devel \
    pcp-selinux pcp-system-tools pcp-testsuite pcp-zeroconf \
    $pmda_packages $import_packages $export_packages ; \
do \
    echo $subpackage >> packages.list; \
done

rm -f *-files.rpm *-tmpfiles.rpm
sort -u $DIST_MANIFEST | awk '
function loadfiles(files) {
    system ("touch " files"-files");
    filelist=files"-files";
    while (getline < filelist) {
        if (length(pkg[$0]) > 0 && pkg[$0] != files)
            print "Dup: ", $0, " package: ", pkg[$0], " and ", files;
        if (length(pkg[$0]) == 0)
            pkg[$0] = files;
    }
}
BEGIN {
    while (getline < "packages.list") loadfiles($0);
    while (getline < "confpath.list") conf[nconf++]=$0;
}
{
    if (pkg[$NF]) p=pkg[$NF];
    else p="pcp";
    f=p"-files.rpm";
}
$1 == "d" {
            if (match ($5, "'$PCP_RUN_DIR'")) {
                printf ("%%%%ghost ") >> f;
            }
            if (match ($5, "'$PCP_VAR_DIR'/testsuite")) {
                $3 = $4 = "pcpqa";
            }
            printf ("%%%%dir %%%%attr(%s,%s,%s) %s\n", $2, $3, $4, $5) >> f
          }
$1 == "f" && $6 ~ "etc/pcp\\.conf" { printf ("%%%%config ") >> f; }
$1 == "f" && $6 ~ "etc/pcp\\.env"  { printf ("%%%%config ") >> f; }
$1 == "f" && $6 ~ "etc/pcp\\.sh"   { printf ("%%%%config ") >> f; }
$1 == "f" {
            for (i=0; i < nconf; i++) {
                if ($6 ~ conf[i]) {
                    printf ("%%%%config(noreplace) ") >> f;
                    break;
                }
            }
            if (match ($6, "'$PCP_VAR_DIR'/testsuite")) {
                $3 = $4 = "pcpqa";
            }
            if (match ($6, "'$PCP_MAN_DIR'") || match ($6, "'$PCP_DOC_DIR'")) {
                printf ("%%%%doc ") >> f;
            }
            printf ("%%%%attr(%s,%s,%s) %s\n", $2, $3, $4, $6) >> f
          }
$1 == "l" {
            if (match ($3, "'$PCP_VAR_DIR'")) {
                print $3 >> p"-tmpfiles";
                if (length(tmpfiles[p]) == 0) {
                    printf ("'$PCP_SYSTEMDTMPFILES_DIR'/%s.conf\n", p) >> f;
                    tmpfiles[p] = p;
                }
            }
            print $3 >> f;
          }'

mkdir -p $DIST_ROOT/$PCP_SYSTEMDTMPFILES_DIR
sort -u $DIST_TMPFILES | awk '
function loadtmpfiles(files) {
    system ("touch " files"-tmpfiles");
    filelist=files"-tmpfiles";
    while (getline < filelist) {
        if (pkg[$0] && pkg[$0] != files)
            print "Dup: ", $0, " package: ", pkg[$0], " and ", files;
        pkg[$0] = files;
    }
}
BEGIN {
    while (getline < "packages.list") loadtmpfiles($0);
}
{
    if (pkg[$2]) p=pkg[$2];
    else p="pcp";
    f=p".conf";
    printf ("%s\n", $0) >> f;
}'

%if %{disable_mssql}
rm -f pcp-pmda-mssql.conf
%endif

for tmpfile in *.conf ; \
do \
    mv $tmpfile $DIST_ROOT/$PCP_SYSTEMDTMPFILES_DIR/$tmpfile; \
done

find %{buildroot} -type f -name '*.so' -exec strip '{}' ';'

%pre devel
test -d %{_testsdir} || mkdir -p -m 755 %{_testsdir}
getent group pcpqa >/dev/null || groupadd -r pcpqa
getent passwd pcpqa >/dev/null || \
  useradd -c "PCP Quality Assurance" -g pcpqa -d %{_testsdir} -M -r -s /bin/bash pcpqa 2>/dev/null
chown -R pcpqa:pcpqa %{_testsdir} 2>/dev/null
exit 0

%post devel
chown -R pcpqa:pcpqa %{_testsdir} 2>/dev/null
systemctl restart pmcd >/dev/null 2>&1
systemctl restart pmlogger >/dev/null 2>&1
systemctl enable pmcd >/dev/null 2>&1
systemctl enable pmlogger >/dev/null 2>&1
exit 0

%pre
getent group pcp >/dev/null || groupadd -r pcp
getent passwd pcp >/dev/null || \
  useradd -c "Performance Co-Pilot" -g pcp -d %{_localstatedir}/lib/pcp -M -r -s /sbin/nologin pcp
exit 0

%preun pmda-systemd
%{pmda_remove "$1" "systemd"}

%preun pmda-infiniband
%{pmda_remove "$1" "infiniband"}

%preun pmda-perfevent
%{pmda_remove "$1" "perfevent"}

%preun pmda-podman
%{pmda_remove "$1" "podman"}

%preun pmda-json
%{pmda_remove "$1" "json"}

%preun pmda-nginx
%{pmda_remove "$1" "nginx"}

%preun pmda-oracle
%{pmda_remove "$1" "oracle"}

%preun pmda-postgresql
%{pmda_remove "$1" "postgresql"}

%preun pmda-postfix
%{pmda_remove "$1" "postfix"}

%preun pmda-elasticsearch
%{pmda_remove "$1" "elasticsearch"}

%preun pmda-openvswitch
%{pmda_remove "$1" "openvswitch"}

%preun pmda-rabbitmq
%{pmda_remove "$1" "rabbitmq"}

%preun pmda-snmp
%{pmda_remove "$1" "snmp"}

%preun pmda-mysql
%{pmda_remove "$1" "mysql"}

%preun pmda-activemq
%{pmda_remove "$1" "activemq"}

%preun pmda-bind2
%{pmda_remove "$1" "bind2"}

%preun pmda-bonding
%{pmda_remove "$1" "bonding"}

%preun pmda-dbping
%{pmda_remove "$1" "dbping"}

%preun pmda-denki
%{pmda_remove "$1" "denki"}

%preun pmda-docker
%{pmda_remove "$1" "docker"}

%preun pmda-ds389
%{pmda_remove "$1" "ds389"}

%preun pmda-ds389log
%{pmda_remove "$1" "ds389log"}

%preun pmda-gpfs
%{pmda_remove "$1" "gpfs"}

%preun pmda-gpsd
%{pmda_remove "$1" "gpsd"}

%preun pmda-lio
%{pmda_remove "$1" "lio"}

%preun pmda-openmetrics
%{pmda_remove "$1" "openmetrics"}

%preun pmda-lustre
%{pmda_remove "$1" "lustre"}

%preun pmda-lustrecomm
%{pmda_remove "$1" "lustrecomm"}

%preun pmda-memcache
%{pmda_remove "$1" "memcache"}

%preun pmda-named
%{pmda_remove "$1" "named"}

%preun pmda-netfilter
%{pmda_remove "$1" "netfilter"}

%preun pmda-news
%{pmda_remove "$1" "news"}

%preun pmda-nfsclient
%{pmda_remove "$1" "nfsclient"}

%preun pmda-nutcracker
%{pmda_remove "$1" "nutcracker"}

%preun pmda-pdns
%{pmda_remove "$1" "pdns"}

%preun pmda-rsyslog
%{pmda_remove "$1" "rsyslog"}

%preun pmda-redis
%{pmda_remove "$1" "redis"}

%preun pmda-samba
%{pmda_remove "$1" "samba"}

%preun pmda-zimbra
%{pmda_remove "$1" "zimbra"}

%preun pmda-dm
%{pmda_remove "$1" "dm"}

%if !%{disable_bcc}
%preun pmda-bcc
%{pmda_remove "$1" "bcc"}
%endif

%preun pmda-bpf
%{pmda_remove "$1" "bpf"}

%preun pmda-bpftrace
%{pmda_remove "$1" "bpftrace"}

%preun pmda-gluster
%{pmda_remove "$1" "gluster"}

%preun pmda-zswap
%{pmda_remove "$1" "zswap"}

%preun pmda-unbound
%{pmda_remove "$1" "unbound"}

%preun pmda-mic
%{pmda_remove "$1" "mic"}

%preun pmda-haproxy
%{pmda_remove "$1" "haproxy"}

%preun pmda-libvirt
%{pmda_remove "$1" "libvirt"}

%preun pmda-lmsensors
%{pmda_remove "$1" "lmsensors"}

%preun pmda-mongodb
%{pmda_remove "$1" "mongodb"}

%if !%{disable_mssql}
%preun pmda-mssql
%{pmda_remove "$1" "mssql"}
%endif

%preun pmda-netcheck
%{pmda_remove "$1" "netcheck"}

%preun pmda-apache
%{pmda_remove "$1" "apache"}

%preun pmda-bash
%{pmda_remove "$1" "bash"}

%preun pmda-cifs
%{pmda_remove "$1" "cifs"}

%preun pmda-cisco
%{pmda_remove "$1" "cisco"}

%preun pmda-gfs2
%{pmda_remove "$1" "gfs2"}

%preun pmda-logger
%{pmda_remove "$1" "logger"}

%preun pmda-mailq
%{pmda_remove "$1" "mailq"}

%preun pmda-mounts
%{pmda_remove "$1" "mounts"}

%preun pmda-nvidia-gpu
%{pmda_remove "$1" "nvidia"}

%preun pmda-roomtemp
%{pmda_remove "$1" "roomtemp"}

%preun pmda-sendmail
%{pmda_remove "$1" "sendmail"}

%preun pmda-shping
%{pmda_remove "$1" "shping"}

%preun pmda-smart
%{pmda_remove "$1" "smart"}

%preun pmda-sockets
%{pmda_remove "$1" "sockets"}

%preun pmda-hacluster
%{pmda_remove "$1" "hacluster"}

%preun pmda-summary
%{pmda_remove "$1" "summary"}

%preun pmda-trace
%{pmda_remove "$1" "trace"}

%preun pmda-weblog
%{pmda_remove "$1" "weblog"}

%preun zeroconf
if [ "$1" -eq 0 ]
then
    %systemd_preun pmlogger_daily_report.timer
    %systemd_preun pmlogger_daily_report.service
fi

%preun
if [ "$1" -eq 0 ]
then
    %systemd_preun pmlogger.service
    %systemd_preun pmlogger_farm.service
    %systemd_preun pmie.service
    %systemd_preun pmie_farm.service
    %systemd_preun pmproxy.service
    %systemd_preun pmcd.service
    %systemd_preun pmie_daily.timer
    %systemd_preun pmlogger_daily.timer
    %systemd_preun pmlogger_check.timer
    %systemd_preun pmlogger_farm_check.timer
    %systemd_preun pmie_farm_check.timer

    systemctl stop pmlogger.service >/dev/null 2>&1
    systemctl stop pmlogger_farm.service >/dev/null 2>&1
    systemctl stop pmie.service >/dev/null 2>&1
    systemctl stop pmie_farm.service >/dev/null 2>&1
    systemctl stop pmproxy.service >/dev/null 2>&1
    systemctl stop pmcd.service >/dev/null 2>&1
    
    PCP_PMNS_DIR=%{_pmnsdir}
    rm -f "$PCP_PMNS_DIR/.NeedRebuild" >/dev/null 2>&1
fi

%post zeroconf
PCP_PMDAS_DIR=%{_pmdasdir}
PCP_SYSCONFIG_DIR=%{_sysconfdir}/sysconfig
PCP_PMCDCONF_PATH=%{_confdir}/pmcd/pmcd.conf
for PMDA in dm nfsclient openmetrics ; do
    if ! grep -q "$PMDA/pmda$PMDA" "$PCP_PMCDCONF_PATH"
    then
        %{install_file "$PCP_PMDAS_DIR/$PMDA" .NeedInstall}
    fi
done
pmieconf -c enable dmthin
systemctl restart pmcd >/dev/null 2>&1
systemctl restart pmlogger >/dev/null 2>&1
systemctl restart pmlogger_farm >/dev/null 2>&1
systemctl restart pmie >/dev/null 2>&1
systemctl restart pmie_farm >/dev/null 2>&1
systemctl enable pmcd >/dev/null 2>&1
systemctl enable pmlogger >/dev/null 2>&1
systemctl enable pmlogger_farm >/dev/null 2>&1
systemctl enable pmie >/dev/null 2>&1
systemctl enable pmie_farm >/dev/null 2>&1

%post selinux
%{selinux_handle_policy "$1" "pcpupstream"}

%triggerin selinux -- docker-selinux
%{selinux_handle_policy "$1" "pcpupstream-docker"}

%triggerin selinux -- container-selinux
%{selinux_handle_policy "$1" "pcpupstream-container"}

%post
PCP_PMNS_DIR=%{_pmnsdir}
PCP_LOG_DIR=%{_logsdir}
%{install_file "$PCP_PMNS_DIR" .NeedRebuild}
%{install_file "$PCP_LOG_DIR/pmlogger" .NeedRewrite}
rm -f %{_sysconfdir}/systemd/system/pm*.requires/pm*-poll.* >/dev/null 2>&1 || true

if systemctl is-enabled pmlogger.service >/dev/null; then
systemctl enable pmlogger_farm.service pmlogger_farm_check.service
systemctl start pmlogger_farm.service pmlogger_farm_check.service
fi
if systemctl is-enabled pmie.service >/dev/null; then
systemctl enable pmie_farm.service pmie_farm_check.service
systemctl start pmie_farm.service pmie_farm_check.service
fi

%systemd_postun_with_restart pmcd.service
%systemd_post pmcd.service
%systemd_postun_with_restart pmlogger.service
%systemd_post pmlogger.service
%systemd_postun_with_restart pmlogger_farm.service
%systemd_post pmlogger_farm.service
%systemd_post pmlogger_farm_check.service
%systemd_postun_with_restart pmie.service
%systemd_post pmie.service
%systemd_postun_with_restart pmie_farm.service
%systemd_post pmie_farm.service
%systemd_post pmie_farm_check.service
systemctl condrestart pmproxy.service >/dev/null 2>&1
%{rebuild_pmns "$PCP_PMNS_DIR" .NeedRebuild}

%ldconfig_scriptlets libs

%preun selinux
%{selinux_handle_policy "$1" "pcpupstream"}

%triggerun selinux -- docker-selinux
%{selinux_handle_policy "$1" "pcpupstream-docker"}

%triggerun selinux -- container-selinux
%{selinux_handle_policy "$1" "pcpupstream-container"}

%files -f pcp-files.rpm
%doc CHANGELOG COPYING INSTALL.md README.md VERSION.pcp pcp.lsm
%ghost %dir %attr(0775,pcp,pcp) %{_localstatedir}/run/pcp

%files conf -f pcp-conf-files.rpm

%files -f pcp-libs-files.rpm

%files devel -f pcp-libs-devel-files.rpm

%files devel -f pcp-devel-files.rpm

%files help -f pcp-help-files.rpm

%files selinux -f pcp-selinux-files.rpm

%files gui -f pcp-gui-files.rpm

%files devel -f pcp-testsuite-files.rpm

%files pmda-infiniband -f pcp-pmda-infiniband-files.rpm

%files pmda-podman -f pcp-pmda-podman-files.rpm

%files pmda-perfevent -f pcp-pmda-perfevent-files.rpm

%files pmda-activemq -f pcp-pmda-activemq-files.rpm

%files pmda-bind2 -f pcp-pmda-bind2-files.rpm

%files pmda-nutcracker -f pcp-pmda-nutcracker-files.rpm

%files pmda-elasticsearch -f pcp-pmda-elasticsearch-files.rpm

%files pmda-redis -f pcp-pmda-redis-files.rpm

%files pmda-bonding -f pcp-pmda-bonding-files.rpm

%files pmda-dbping -f pcp-pmda-dbping-files.rpm

%files pmda-ds389log -f pcp-pmda-ds389log-files.rpm

%files pmda-ds389 -f pcp-pmda-ds389-files.rpm

%files pmda-gpfs -f pcp-pmda-gpfs-files.rpm

%files pmda-gpsd -f pcp-pmda-gpsd-files.rpm

%files pmda-lustre -f pcp-pmda-lustre-files.rpm

%files pmda-memcache -f pcp-pmda-memcache-files.rpm

%files pmda-named -f pcp-pmda-named-files.rpm

%files pmda-netfilter -f pcp-pmda-netfilter-files.rpm

%files pmda-news -f pcp-pmda-news-files.rpm

%files pmda-pdns -f pcp-pmda-pdns-files.rpm

%files pmda-rsyslog -f pcp-pmda-rsyslog-files.rpm

%files pmda-samba -f pcp-pmda-samba-files.rpm

%files pmda-slurm -f pcp-pmda-slurm-files.rpm

%files pmda-zimbra -f pcp-pmda-zimbra-files.rpm

%files pmda-denki -f pcp-pmda-denki-files.rpm

%files pmda-docker -f pcp-pmda-docker-files.rpm

%files pmda-lustrecomm -f pcp-pmda-lustrecomm-files.rpm

%files pmda-mysql -f pcp-pmda-mysql-files.rpm

%files pmda-nginx -f pcp-pmda-nginx-files.rpm

%files pmda-postfix -f pcp-pmda-postfix-files.rpm

%files pmda-postgresql -f pcp-pmda-postgresql-files.rpm

%files pmda-oracle -f pcp-pmda-oracle-files.rpm

%files pmda-snmp -f pcp-pmda-snmp-files.rpm

%files pmda-dm -f pcp-pmda-dm-files.rpm

%if !%{disable_bcc}
%files pmda-bcc -f pcp-pmda-bcc-files.rpm
%endif

%files pmda-bpf -f pcp-pmda-bpf-files.rpm

%files pmda-bpftrace -f pcp-pmda-bpftrace-files.rpm

%files pmda-gluster -f pcp-pmda-gluster-files.rpm

%files pmda-zswap -f pcp-pmda-zswap-files.rpm

%files pmda-unbound -f pcp-pmda-unbound-files.rpm

%files pmda-mic -f pcp-pmda-mic-files.rpm

%files pmda-haproxy -f pcp-pmda-haproxy-files.rpm

%files pmda-lmsensors -f pcp-pmda-lmsensors-files.rpm

%files pmda-mongodb -f pcp-pmda-mongodb-files.rpm

%if !%{disable_mssql}
%files pmda-mssql -f pcp-pmda-mssql-files.rpm
%endif

%files pmda-netcheck -f pcp-pmda-netcheck-files.rpm

%files pmda-nfsclient -f pcp-pmda-nfsclient-files.rpm

%files pmda-openvswitch -f pcp-pmda-openvswitch-files.rpm

%files pmda-rabbitmq -f pcp-pmda-rabbitmq-files.rpm

%files export-pcp2graphite -f pcp-export-pcp2graphite-files.rpm

%files export-pcp2json -f pcp-export-pcp2json-files.rpm

%files export-pcp2spark -f pcp-export-pcp2spark-files.rpm

%files export-pcp2xml -f pcp-export-pcp2xml-files.rpm

%files export-pcp2zabbix -f pcp-export-pcp2zabbix-files.rpm

%files export-pcp2elasticsearch -f pcp-export-pcp2elasticsearch-files.rpm

%files export-pcp2influxdb -f pcp-export-pcp2influxdb-files.rpm

%files export-zabbix-agent -f pcp-export-zabbix-agent-files.rpm

%files pmda-json -f pcp-pmda-json-files.rpm

%files pmda-libvirt -f pcp-pmda-libvirt-files.rpm

%files pmda-lio -f pcp-pmda-lio-files.rpm

%files pmda-openmetrics -f pcp-pmda-openmetrics-files.rpm

%files pmda-apache -f pcp-pmda-apache-files.rpm

%files pmda-bash -f pcp-pmda-bash-files.rpm

%files pmda-cifs -f pcp-pmda-cifs-files.rpm

%files pmda-cisco -f pcp-pmda-cisco-files.rpm

%files pmda-gfs2 -f pcp-pmda-gfs2-files.rpm

%files pmda-logger -f pcp-pmda-logger-files.rpm

%files pmda-mailq -f pcp-pmda-mailq-files.rpm

%files pmda-mounts -f pcp-pmda-mounts-files.rpm

%files pmda-nvidia-gpu -f pcp-pmda-nvidia-files.rpm

%files pmda-roomtemp -f pcp-pmda-roomtemp-files.rpm

%files pmda-sendmail -f pcp-pmda-sendmail-files.rpm

%files pmda-shping -f pcp-pmda-shping-files.rpm

%files pmda-smart -f pcp-pmda-smart-files.rpm

%files pmda-sockets -f pcp-pmda-sockets-files.rpm

%files pmda-hacluster -f pcp-pmda-hacluster-files.rpm

%files pmda-summary -f pcp-pmda-summary-files.rpm

%files pmda-systemd -f pcp-pmda-systemd-files.rpm

%files pmda-trace -f pcp-pmda-trace-files.rpm

%files pmda-weblog -f pcp-pmda-weblog-files.rpm

%files import-sar2pcp -f pcp-import-sar2pcp-files.rpm

%files import-iostat2pcp -f pcp-import-iostat2pcp-files.rpm

%files import-mrtg2pcp -f pcp-import-mrtg2pcp-files.rpm

%files import-ganglia2pcp -f pcp-import-ganglia2pcp-files.rpm

%files import-collectl2pcp -f pcp-import-collectl2pcp-files.rpm

%files -n perl-PCP-PMDA -f perl-pcp-pmda.list

%files -n perl-PCP-MMV -f perl-pcp-mmv.list

%files -n perl-PCP-LogImport -f perl-pcp-logimport.list

%files -n perl-PCP-LogSummary -f perl-pcp-logsummary.list

%files -n python3-pcp -f python3-pcp.list.rpm

%files system-tools -f pcp-system-tools-files.rpm

%files zeroconf -f pcp-zeroconf-files.rpm


%changelog
* Thu Dec 22 2022 wanglin <wangl29@chinatelecom.cn> - 5.3.7-2
- Fix rpm-config hard code problem

* Tue Jul 26 2022 tianlijing <tianlijing@kylinos.cn> - 5.3.7-1
- Update to 5.3.7

* Wed Apr 06 2022 wangkai <wangkai385@huawei.com> - 5.3.5-1
- Update to 5.3.5

* Tue Nov 02 2021 lingsheng <lingsheng@huawei.com> - 4.1.3-17
- Add pcp_domain selinux policy

* Wed Aug 04 2021 wangyong <wangyong187@huawei.com> - 4.1.3-16
- Fix build error caused by GCC upgrade to GCC-10

* Mon Nov 09 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-15
- Change require to python3-bpfcc

* Wed Oct 21 2020 wangxiao <wangxiao65@huawei.com> - 4.1.3-14
- drop python2 subpackage

* Tue Sep 22 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-13
- fix pcp pidstat bug  -a option

* Fri Sep 18 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 4.1.3-12
- Fix some bugs in pcp-pidstat,include -U option error,-? need an arg,
  And del unused -h/--host options

* Thu Sep 17 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-11
- Fix collect2pcp option

* Thu Sep 16 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-10
- Fix some options and syntax errors

* Fri Aug 28 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-9
- Fix .NeedRebuild unfound when removing rpm

* Tue Aug 04 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-8
- Fix compile failure in make multithreading

* Tue Jul 28 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-7
- change require to rdma-core as libibmad and libibumad obsoleted

* Wed Jul 15 2020 huanghaitao <huanghaitao@huawei.com> - 4.1.3-6
- change undelete rebuild flag file .NeedRebuild

* Tue Jun 23 2020 panchenbo <panchenbo@uniontech.com> - 4.1.3-5
- Type:bugfix
- ID: NA
- SUG: install
- DESC: Fix pcp-pmda-activemq install error

* Wed May 13 2020 huanghaitao <huanghaitao8@huawei.com> - 4.1.3-4
- Type:cves
- ID: CVE-2019-3695 CVE-2019-3696 
- SUG:restart
- DESC: fix CVE-2019-3695 CVE-2019-3696

* Fri Feb 21 2020 Senlin Xia <xiasenlin1@huawei.com> - 4.1.3-3
- package init
